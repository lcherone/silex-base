<?php
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../src/config.php';
require_once __DIR__.'/../src/bootstrap.php';
require_once __DIR__.'/../src/routes.php';

$app->run();