<?php
namespace App\Lib;

class TypeHint
{
    public static function initializeHandler()
    {
        set_error_handler('App\Lib\TypeHint::phpErrorHandler');
        return true;
    }

    public static function phpErrorHandler($errno, $errstr, $errfile, $errline)
    {
        if (error_reporting() & $errno) {
            if ($errno == E_RECOVERABLE_ERROR) {
                $regexp = '/^Argument (\d)+ passed to (.+) must be an instance of (?<hint>.+), (?<given>.+) given/i';

                if (preg_match($regexp, $errstr, $match)) {
                    $given = $match['given'];

                    $nsHint = explode('\\', $match['hint']);
                    $hint  = end($nsHint) ; // namespace support.

                    if ($hint === "int") {
                        $hint = "integer";
                    } elseif ($hint === "float") {
                        $hint = "double";
                    } elseif ($hint === "string") {
                        $hint = "string";
                    }

                    if ($hint === $given) {
                        return true;
                    }

                    $newerrstr = str_replace($match['hint'], $hint, $errstr);

                    throw new \Exception($newerrstr);
                }
            }
            return false;
        }
    }
}
