<?php
namespace App\Controller;

use App\Model;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class userController
 *
 * @package App\Controller
 * @author  Lawrence Cherone <lawrence@cherone.co.uk>
 */
class userController
{
    /**
     * index Page
     *
     * @param Request     $request
     * @param Application $app
     *
     * @return mixed
     */
    public function indexAction(Request $request, Application $app)
    {
        return $app->redirect('/user/account');
    }

    /**
     * register user action
     */
    public function registerAction(Request $request, Application $app)
    {
        // check already logged in
        if (null !== $app['user'] = $app['session']->get('user')) {
            return $app->redirect('/user/account');
        }

        // load user model
        $app['model.user'] = $app->share(function () use ($app) {
            return new Model\userModel($app);
        });

        // create register form
        $form = $app['form.factory']->createBuilder('form')
            ->add('name', 'text', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('username', 'text', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->add('email', 'text', array(
                'required' => true,
                'constraints' => array(new Assert\Email(), new Assert\NotBlank())
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'first_options'  => array('label' => 'Password', 'error_bubbling' => true),
                'second_options' => array('label' => 'Repeat Password', 'error_bubbling' => true),
                'error_bubbling'    =>  true
            ))
            ->getForm();

        // handle POST
        if ('POST' == $request->getMethod()) {

            $form->bind($request);

            if ($form->isValid()) {

                $data = $form->getData();

                // check user exists
                if(null !== $result = $app['model.user']->getByEmail($data['email'])) {
                    $app['session']->getFlashBag()->set('alert', 'A user with that email already exists');
                } else {
                    // insert user
                    $user_id = $app['model.user']->add(array(
                       'name' => $data['name'],
                       'username' => $data['username'],
                       'email' => $data['email'],
                       'user_level'=>10,
                       'password' => password_hash(
                            $data['password'],
                            PASSWORD_BCRYPT,
                            array("cost" => 10)
                        ),
                    ));

                    // add account
                    $app['model.account'] = $app->share(function () use ($app) {
                        return new Model\accountModel($app);
                    });
                    $account_id = $app['model.account']->add(array(
                        'user_id' => $user_id,
                    ));

                    // update account_id on user row
                    $app['model.user']->updateAccountId($user_id, $account_id);

                    $app['session']->getFlashBag()->set('alert', 'Account created, go ahead and login');
                    return $app->redirect('/login');
                }
            }
        }

        return $app['twig']->render('user/register.twig', array(
            'form'  => $form->createView(),
        ));
    }

    public function loginAction(Request $request, Application $app)
    {
        // check already logged in
        if (null !== $app['user'] = $app['session']->get('user')) {
            return $app->redirect('/user/account');
        }

        // load user model
        $app['model.user'] = $app->share(function () use ($app) {
            return new Model\userModel($app);
        });

        // create login form
        $form = $app['form.factory']->createBuilder('form')
            ->add('email', 'text', array(
                'required' => true,
                'constraints' => array(new Assert\Email(), new Assert\NotBlank())
            ))
            ->add('password', 'password', array(
                'required' => true,
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 6)))
            ))
            ->getForm();

        // handle POST
        if ('POST' == $request->getMethod()) {

            $form->bind($request);

            if ($form->isValid()) {

                $data = $form->getData();

                if(null !== $result = $app['model.user']->getByEmail($data['email'])) {
                    if (password_verify($data['password'], $result['password'])) {
                        $app['session']->set('logged_in', true);
                        $app['session']->set('user', $result);
                        return $app->redirect('/user/account');
                    } else {
                        // set a bootstrap alert
                        $app['session']->getFlashBag()->set('alert', 'Incorrect username or password');
                    }
                } else {
                    // set a bootstrap alert
                    $app['session']->getFlashBag()->set('alert', 'Incorrect username or password');
                }
            }
        }

        return $app['twig']->render('user/login.twig', array(
            'form'  => $form->createView(),
        ));
    }

    public function logoutAction(Request $request, Application $app)
    {
        $app['session']->clear();
        $app['session']->getFlashBag()->set('alert', 'Successfully logged out');
        return $app->redirect('/login');
    }

    public function accountAction(Request $request, Application $app)
    {
        if (null === $app['user'] = $app['session']->get('user')) {
            $app['session']->getFlashBag()->set('alert', 'You must be logged in to do that');
            return $app->redirect('/login');
        }

        // render and return view
        return $app['twig']->render(
            'user/account.twig',
            array(
                'user' => $app['user'],
            )
        );
    }

}
