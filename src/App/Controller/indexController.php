<?php
namespace App\Controller;

use App\Model;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class indexController
 *
 * @package App\Controller
 * @author Lawrence Cherone <lawrence@cherone.co.uk>
 */
class indexController
{
    /**
     * index Page
     *
     * @param Request     $request
     * @param Application $app
     *
     * @return mixed
     */
    public function indexAction(Request $request, Application $app)
    {
        // create a singleton of the testModel class
        $app['model.test'] = $app->share(function () use ($app) {
            return new Model\testModel($app);
        });

        // set a bootstrap alert
        $app['session']->getFlashBag()->set('alert', 'Alerts\'N\'all');

        // render and return view
        return $app['twig']->render(
            'index/index.twig',
            array(
                'foo' => $request->get('foo'),
                'data' => $app['model.test']->getAll(),
                'single' => $app['model.test']->getByKey('foo')
            )
        );
    }

}
