<?php
namespace App\Model;

use Silex\Application;

use DateTime;
use R;

/**
 * Class userModel
 *
 * @package App\Model
 * @author Lawrence Cherone <lawrence@cherone.co.uk>
 */
class userModel
{
    /**
     * construct
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Add user
     *
     * @param  array $data
     * @return int
     */
    public function add(array $data)
    {
        $user = R::dispense('user');

        $user->account_id = 0;
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->total_logins = 0;
        $user->user_level = $data['user_level'];
        $user->created_time = time();
        $user->deleted_time = 0;
        $user->deleted_reason = null;
        $user->changepass = 0;
        $user->resetcode = null;

        return R::store($user);
    }

    /**
     * Update Account Id
     *
     * @param  int $user_id
     * @param  int $account_id
     * @return mixed
     */
    public function updateAccountId(int $user_id, int $account_id)
    {
        if (null !== $user = $this->getById($user_id)) {
            $user->account_id = $account_id;
            return R::store($user);
        }
        return false;
    }

    public function getByEmail(string $email)
    {
        return R::findOne('user', ' `email` = ? AND deleted_time = 0', array($email));
    }

    public function getById(int $id)
    {
        return R::findOne('user', ' `id` = ? ', array($id));
    }

    public function getAll()
    {
        return R::getAll('SELECT * FROM user');
    }

    // ...
}
