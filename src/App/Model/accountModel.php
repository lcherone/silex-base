<?php
namespace App\Model;

use Silex\Application;

use DateTime;
use R;

/**
 * Class accountModel
 *
 * @package App\Model
 * @author Lawrence Cherone <lawrence@cherone.co.uk>
 */
class accountModel
{
    /**
     * construct
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function add(array $data)
    {
        $account = R::dispense('account');

        $account->system_id = 0;
        $account->user_id = $data['user_id'];
        $account->created_time = time();
        $account->updated_time = time();
        $account->deleted_time = 0;

        return R::store($account);
    }

    // ...
}
