<?php
namespace App\Model;

use Silex\Application;

use DateTime;
use R;

/**
 * Class testModel
 *
 * @package App\Model
 * @author Lawrence Cherone <lawrence@cherone.co.uk>
 */
class testModel
{
    /**
     * construct
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get bean by key
     *
     * @param string $key
     * @return object
     */
    public function getByKey(string $key)
    {
        return R::findOne('test', ' `key` = ? ', array($key));
    }

    public function getAll()
    {
        $this->test = R::getAll('SELECT * FROM test');

        if (empty($this->test)) {
            //init a new article bean/row
            $this->test = R::dispense('test');

            $date = new DateTime();
            $this->test->date = $date->format('g:i a \o\n l jS \of F Y');

            $this->test->key   = 'foo';
            $this->test->value = 'bar';

            R::store($this->test);
        }

        return $this->test;
    }

    // ...
}
