<?php
/**
 * Error Reporting
 */
error_reporting(E_ALL);
ini_set('display_errors', true);

define('SITE_NAME' , 'A Silex Base App');

define('DEBUG' , true);

/**
 * Database
 */
$database = array(
    'hostname'=>'127.0.0.1',
    'database'=>'c9',
    'username'=>'lcherone',
    'password'=>''
);

/* & connect to redbean database - because I want too */
R::setup(
    'mysql:host='.$database['hostname'].';dbname='.$database['database'],
    $database['username'],
    $database['password']
);
