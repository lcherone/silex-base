<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

use Silex\Application;
use Silex\Provider;
use App\Lib\TypeHint;

// scalar typhinting patch
TypeHint::initializeHandler();

// init silex application and dependencies
$app = new Application();

$app->register(new Provider\FormServiceProvider());
$app->register(new Provider\ValidatorServiceProvider());
$app->register(new Provider\TranslationServiceProvider());
$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\UrlGeneratorServiceProvider());
$app->register(
    new Provider\TwigServiceProvider(),
    array(
        'twig.path' => __DIR__.'/App/View'
    )
);

/**
 * twig globals
 */
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
  $twig->addGlobal('site_name', SITE_NAME);
  $twig->addGlobal('user', $app['session']->get('user'));
  //...
  return $twig;
}));

$app['debug'] = DEBUG;

// error Handler
$app->error(function (\Exception $e, $code) use ($app) {

    if ($app['debug']) {
        $message = 'Error Message: ' . $e->getMessage();
    }

    switch ($code) {
        case 404:
            $message = $app['twig']->render('error404.twig', array('error_msg' => $message));
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong. ('.$message.')';
    }

    return new Response($message, $code);
});
