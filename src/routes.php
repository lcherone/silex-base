<?php
/**
 * controller routes
 */

// index
$app->get('/', 'App\Controller\indexController::indexAction')->bind('index');

// login
$app->match('/login',  'App\Controller\userController::loginAction')->bind('login');
$app->match('/logout', 'App\Controller\userController::logoutAction')->bind('logout');

// user
$app->get('/user',            'App\Controller\userController::indexAction')->bind('user');
$app->match('/user/register', 'App\Controller\userController::registerAction')->bind('user.register');
$app->get('/user/account',    'App\Controller\userController::accountAction')->bind('user.account');


